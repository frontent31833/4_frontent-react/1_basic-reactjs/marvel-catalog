# React + TypeScript + Vite => avengers web desing

## preview

desktop preview
![Alt text](https://res.cloudinary.com/dhq9acwqr/image/upload/v1698876273/avengers/templates/kanzdvyxvxcxtvqvkyyf.jpg)
![Alt text](https://res.cloudinary.com/dhq9acwqr/image/upload/v1698876274/avengers/templates/gncceugkpdmvxxhhzca3.png)

mobile preview
![Alt text](https://res.cloudinary.com/dhq9acwqr/image/upload/v1698876276/avengers/templates/euiezeulirszxawgtu46.jpg)

[template](https://www.behance.net/gallery/64907137/Avengers-Infinity-War)

- tecnologias:

  - vite: servidor de desarrollo local
  - react: interfaces de usuario
  - material UI : stylos y diseño
  - clodinary : imagenes

- clodynary formato de subida de archivos

```js
cloudinary.v2.uploader
  .upload("spiderman.png", {
    folder: "avengers/templates",
    resource_type: "image",
  })
  .then(console.log);
```
