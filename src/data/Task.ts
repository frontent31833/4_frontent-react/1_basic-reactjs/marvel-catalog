import { Team } from "./Team";
export interface TaskModel {
  rating: number;
  title: string;
  categori: Category[];
  body: string;
  members: Team[];
  totalTime: number;
}

interface Category {
  title: string;
}

export const TasckData: TaskModel[] = [
  {
    rating: 1,
    title: "Design UI",
    categori: [
      {
        title: "Design",
      },
      {
        title: "Testing",
      },
    ],
    body: "Create a user interface design for the software.",
    members: [
      {
        name: "John Doe",
        job: "Software Engineer",
        pathImg:
          "/alchemyrefiner_alchemymagic_1_25460ca9-b43b-48f8-a9ac-a8be7a76cde8_0.jpg",
        completedTask: 7,
        totalTask: 10,
        status: "online",
      },
      {
        name: "Jane Smith",
        job: "UI/UX Designer",
        pathImg:
          "/Default_Sigyn_and_Loki_the_king_and_queen_of_Asgard_locked_in_1_facdc743-df44-402d-b48c-0bbde35424e2_1.jpg",
        completedTask: 5,
        totalTask: 8,
        status: "offline",
      },
      {
        name: "Mike Johnson",
        job: "Backend Developer",
        pathImg:
          "/DreamShaper_v7_avengers_infinity_war_space_gem_in_dark_environ_0.jpg",
        completedTask: 9,
        totalTask: 12,
        status: "online",
      },
    ],
    totalTime: 8,
  },
  {
    rating: 2,
    title: "Develop Backend",
    categori: [
      {
        title: "Development",
      },
      {
        title: "Code Review",
      },
    ],
    body: "Implement the backend functionality of the software.",
    members: [
      {
        name: "Sarah Williams",
        job: "Frontend Developer",
        pathImg:
          "/DreamShaper_v7_avengers_infinity_war_space_gem_in_dark_environ_1.jpg",
        completedTask: 6,
        totalTask: 9,
        status: "online",
      },
      {
        name: "David Brown",
        job: "Full Stack Developer",
        pathImg:
          "/DreamShaper_v7_avengers_infinity_war_space_gem_in_dark_environ_2.jpg",
        completedTask: 8,
        totalTask: 10,
        status: "offline",
      },
      {
        name: "Emily Davis",
        job: "Quality Assurance",
        pathImg:
          "/DreamShaper_v7_avengers_infinity_war_space_gem_in_dark_environ_3.jpg",
        completedTask: 4,
        totalTask: 7,
        status: "online",
      },
      {
        name: "Chris Wilson",
        job: "Project Manager",
        pathImg: "/DreamShaper_v7_iron_man_0.jpg",
        completedTask: 10,
        totalTask: 10,
        status: "offline",
      },
    ],
    totalTime: 16,
  },
  {
    rating: 3,
    title: "Write Tests",
    categori: [
      {
        title: "Testing",
      },
      {
        title: "Design",
      },
    ],
    body: "Create test cases to ensure the software functions correctly.",
    members: [
      {
        name: "Jessica Thompson",
        job: "Scrum Master",
        pathImg:
          "/DreamShaper_v7_the_space_gem_from_avengers_infinity_war_in_a_d_1.jpg",
        completedTask: 7,
        totalTask: 9,
        status: "offline",
      },
      {
        name: "Ryan Martinez",
        job: "DevOps Engineer",
        pathImg:
          "/DreamShaper_v7_the_space_gem_from_avengers_infinity_war_in_a_d_3.jpg",
        completedTask: 6,
        totalTask: 8,
        status: "online",
      },
      {
        name: "Olivia Anderson",
        job: "Data Analyst",
        pathImg: "/PhotoReal_black_widow_avengers_2.jpg",
        completedTask: 3,
        totalTask: 6,
        status: "offline",
      },
    ],
    totalTime: 4,
  },
  {
    rating: 4,
    title: "Review Code",
    categori: [
      {
        title: "Code Review",
      },
      {
        title: "Development",
      },
    ],
    body: "Check the code for errors and provide feedback for improvement.",
    members: [
      {
        name: "Jane Smith",
        job: "UI/UX Designer",
        pathImg: "/PhotoReal_black_widow_avengers_2.jpg",
        completedTask: 2,
        totalTask: 4,
        status: "offline",
      },
      {
        name: "Michael Johnson",
        job: "Backend Developer",
        pathImg: "/DreamShaper_v7_iron_man_1.jpg",
        completedTask: 4,
        totalTask: 6,
        status: "online",
      },
      {
        name: "Emily Davis",
        job: "Frontend Developer",
        pathImg:
          "/DreamShaper_v7_avengers_infinity_war_space_gem_in_dark_environ_2.jpg",
        completedTask: 1,
        totalTask: 3,
        status: "offline",
      },
      {
        name: "David Wilson",
        job: "Full Stack Developer",
        pathImg: "/PhotoReal_black_widow_avengers_3.jpg",
        completedTask: 5,
        totalTask: 7,
        status: "online",
      },
    ],
    totalTime: 2,
  },
  {
    rating: 5,
    title: "Fix Bugs",
    categori: [
      {
        title: "Bug Fixing",
      },
      {
        title: "Documentation",
      },
    ],
    body: "Identify and resolve any issues or glitches in the software.",
    members: [
      {
        name: "John Doe",
        job: "Software Engineer",
        pathImg:
          "/Default_Sigyn_and_Loki_the_king_and_queen_of_Asgard_locked_in_1_facdc743-df44-402d-b48c-0bbde35424e2_1.jpg",
        completedTask: 4,
        totalTask: 8,
        status: "online",
      },
      {
        name: "Jane Smith",
        job: "UI/UX Designer",
        pathImg:
          "/DreamShaper_v7_avengers_infinity_war_space_gem_in_dark_environ_0.jpg",
        completedTask: 6,
        totalTask: 10,
        status: "offline",
      },
      {
        name: "Mike Johnson",
        job: "Backend Developer",
        pathImg:
          "/DreamShaper_v7_avengers_infinity_war_space_gem_in_dark_environ_1.jpg",
        completedTask: 3,
        totalTask: 5,
        status: "online",
      },
      {
        name: "Sara Wilson",
        job: "Frontend Developer",
        pathImg:
          "/DreamShaper_v7_avengers_infinity_war_space_gem_in_dark_environ_2.jpg",
        completedTask: 7,
        totalTask: 9,
        status: "offline",
      },
      {
        name: "Chris Thompson",
        job: "Full Stack Developer",
        pathImg:
          "/DreamShaper_v7_avengers_infinity_war_space_gem_in_dark_environ_3.jpg",
        completedTask: 2,
        totalTask: 7,
        status: "online",
      },
      {
        name: "Amy Roberts",
        job: "Quality Assurance",
        pathImg:
          "/DreamShaper_v7_avengers_infinity_war_space_gem_in_dark_environ_2.jpg",
        completedTask: 5,
        totalTask: 6,
        status: "offline",
      },
    ],
    totalTime: 6,
  },
  {
    rating: 6,
    title: "Optimize Performance",
    categori: [
      {
        title: "Performance",
      },
      {
        title: "Communication",
      },
    ],
    body: "Improve the speed and efficiency of the software.",
    members: [
      {
        name: "John Doe",
        job: "Software Engineer",
        pathImg:
          "/alchemyrefiner_alchemymagic_1_25460ca9-b43b-48f8-a9ac-a8be7a76cde8_0.jpg",
        completedTask: 10,
        totalTask: 15,
        status: "online",
      },
      {
        name: "Jane Smith",
        job: "UI/UX Designer",
        pathImg:
          "/Default_Sigyn_and_Loki_the_king_and_queen_of_Asgard_locked_in_1_facdc743-df44-402d-b48c-0bbde35424e2_1.jpg",
        completedTask: 8,
        totalTask: 12,
        status: "offline",
      },
      {
        name: "Mark Johnson",
        job: "Backend Developer",
        pathImg:
          "/DreamShaper_v7_avengers_infinity_war_space_gem_in_dark_environ_0.jpg",
        completedTask: 12,
        totalTask: 20,
        status: "online",
      },
      {
        name: "Sarah Williams",
        job: "Frontend Developer",
        pathImg:
          "/DreamShaper_v7_avengers_infinity_war_space_gem_in_dark_environ_1.jpg",
        completedTask: 6,
        totalTask: 10,
        status: "offline",
      },
    ],
    totalTime: 12,
  },
  {
    rating: 7,
    title: "Document Features",
    categori: [
      {
        title: "Documentation",
      },
      {
        title: "Bug Fixing",
      },
    ],
    body: "Create detailed documentation for the software's features.",
    members: [
      {
        name: "John Doe",
        job: "Backend Developer",
        pathImg: "/PhotoReal_Black_Panther_avengers_1.jpg",
        completedTask: 10,
        totalTask: 15,
        status: "Online",
      },
      {
        name: "Jane Smith",
        job: "Frontend Developer",
        pathImg: "/DreamShaper_v7_iron_man_0.jpg",
        completedTask: 8,
        totalTask: 12,
        status: "Offline",
      },
      {
        name: "Mike Johnson",
        job: "Full Stack Developer",
        pathImg:
          "/DreamShaper_v7_the_space_gem_from_avengers_infinity_war_in_a_d_2.jpg",
        completedTask: 12,
        totalTask: 20,
        status: "Online",
      },
      {
        name: "Emily Davis",
        job: "UI/UX Designer",
        pathImg: "/PhotoReal_black_widow_avengers_1.jpg",
        completedTask: 6,
        totalTask: 10,
        status: "Offline",
      },
    ],
    totalTime: 4,
  },
  {
    rating: 8,
    title: "Coordinate with Clients",
    categori: [
      {
        title: "Communication",
      },
      {
        title: "Performance",
      },
    ],
    body: "Communicate with clients to gather requirements and provide updates.",
    members: [
      {
        name: "John Doe",
        job: "Software Engineer",
        pathImg: "/PhotoReal_Black_Panther_avengers_1.jpg",
        completedTask: 3,
        totalTask: 5,
        status: "online",
      },
      {
        name: "Jane Smith",
        job: "UI/UX Designer",
        pathImg:
          "/DreamShaper_v7_avengers_infinity_war_space_gem_in_dark_environ_3.jpg",
        completedTask: 4,
        totalTask: 6,
        status: "offline",
      },
      {
        name: "Michael Johnson",
        job: "Backend Developer",
        pathImg:
          "/DreamShaper_v7_the_space_gem_from_avengers_infinity_war_in_a_d_3.jpg",
        completedTask: 2,
        totalTask: 4,
        status: "online",
      },
      {
        name: "Emily Davis",
        job: "Frontend Developer",
        pathImg: "/PhotoReal_black_widow_avengers_3.jpg",
        completedTask: 5,
        totalTask: 7,
        status: "offline",
      },
      {
        name: "David Wilson",
        job: "Full Stack Developer",
        pathImg:
          "DreamShaper_v7_avengers_infinity_war_space_gem_in_dark_environ_2.jpg",
        completedTask: 1,
        totalTask: 3,
        status: "online",
      },
    ],
    totalTime: 10,
  },
  {
    rating: 9,
    title: "Deploy to Production",
    categori: [
      {
        title: "Deployment",
      },
      {
        title: "Performance",
      },
    ],
    body: "Release the software to the production environment.",
    members: [
      {
        name: "John Doe",
        job: "Software Engineer",
        pathImg:
          "/DreamShaper_v7_the_space_gem_from_avengers_infinity_war_in_a_d_2.jpg",
        completedTask: 3,
        totalTask: 5,
        status: "online",
      },
      {
        name: "Jane Smith",
        job: "UI/UX Designer",
        pathImg: "/DreamShaper_v7_iron_man_0 (1).jpg",
        completedTask: 4,
        totalTask: 6,
        status: "offline",
      },
      {
        name: "Mark Johnson",
        job: "Backend Developer",
        pathImg:
          "https://res.cloudinary.com/dhq9acwqr/image/upload/v1691602446/nft/Default_35mm_F28_insanely_hyperdetailed_and_intricate_realisti_4_8ded548d-2341-46a5-90c6-8a42582b43da_1_wxjvlx.jpg",
        completedTask: 2,
        totalTask: 4,
        status: "online",
      },
      {
        name: "Emily Davis",
        job: "Frontend Developer",
        pathImg: "/DreamShaper_v7_iron_man_0 (1).jpg",
        completedTask: 5,
        totalTask: 7,
        status: "offline",
      },
      {
        name: "Michael Brown",
        job: "Full Stack Developer",
        pathImg:
          "https://res.cloudinary.com/dhq9acwqr/image/upload/v1691602443/nft/Default_Waist_high_Portrait_of_an_exotic_beautiful_caucasian_w_0_91c9fd46-e4c7-41c2-bf37-a81273918d71_1_p3ei69.jpg",
        completedTask: 1,
        totalTask: 3,
        status: "online",
      },
    ],
    totalTime: 2,
  },
  {
    rating: 2,
    title: "Provide Support",
    categori: [
      {
        title: "Support",
      },
      {
        title: "Communication",
      },
    ],
    body: "Assist users with any issues or questions regarding the software.",
    members: [
      {
        name: "John Doe",
        job: "Software Engineer",
        pathImg:
          "https://res.cloudinary.com/dhq9acwqr/image/upload/v1691602446/nft/Default_35mm_F28_insanely_hyperdetailed_and_intricate_realisti_4_8ded548d-2341-46a5-90c6-8a42582b43da_1_wxjvlx.jpg",
        completedTask: 3,
        totalTask: 5,
        status: "online",
      },
      {
        name: "Jane Smith",
        job: "UI/UX Designer",
        pathImg:
          "https://res.cloudinary.com/dhq9acwqr/image/upload/v1691602431/nft/Default_a_full_body_picture_of_a_pretty_high_school_teen_wear_1_ddd272d5-62a6-43c3-bfb0-db00e85a81c1_1_h29xne.jpg",
        completedTask: 4,
        totalTask: 6,
        status: "offline",
      },
      {
        name: "Michael Johnson",
        job: "Backend Developer",
        pathImg:
          "https://res.cloudinary.com/dhq9acwqr/image/upload/v1691598454/nft/Default_hyperrealistic_powerful_whitearmored_female_warrior_h_0_9d60cece-3be8-4ec9-aece-5560bdd5b8a0_1_qt2n7m.jpg",
        completedTask: 2,
        totalTask: 4,
        status: "online",
      },
      {
        name: "Emily Davis",
        job: "Frontend Developer",
        pathImg:
          "https://res.cloudinary.com/dhq9acwqr/image/upload/v1691598451/nft/Default_Maximaliset_Rococo_Astral_Escape_Splash_art_portrait_o_1_e2e0b95e-0ee5-4cc0-a9ad-d6c6728c59da_1_qpf9sr.jpg",
        completedTask: 5,
        totalTask: 7,
        status: "online",
      },
      {
        name: "David Wilson",
        job: "Full Stack Developer",
        pathImg:
          "https://res.cloudinary.com/dhq9acwqr/image/upload/v1691598440/nft/Default_megapolis_cyberpunk_lighting_bar_single_girl_with_head_1_19f3a4d7-9993-44f6-b48e-71f67c289e02_1_lvtpfa.jpg",
        completedTask: 1,
        totalTask: 3,
        status: "offline",
      },
    ],
    totalTime: 6,
  },
];
