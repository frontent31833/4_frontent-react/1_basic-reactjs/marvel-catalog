export interface ProfileModel {
  name: string;
  avatar: string;
  jobtitle: string;
}

export const Profile: ProfileModel = {
  name: "Catherine Pike",
  avatar: "/PhotoReal_Black_Panther_avengers_1.jpg",
  jobtitle: "Software Developer",
};
