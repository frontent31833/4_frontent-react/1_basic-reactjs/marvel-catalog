import { Team, TeamDate } from "./Team";
export interface Project {
  task: string;
  amountTaskDone: number;
  done: boolean;
  members: Team[];
}

export const ProjectData: Project[] = [
  {
    task: "Fix Urgent Mobile App Bugs",
    amountTaskDone: 100,
    done: true,
    members: TeamDate,
  },
  {
    task: "Replace Placeholder Images",
    amountTaskDone: 80,
    done: false,
    members: TeamDate,
  },
  {
    task: "BloomUI Redesign Project",
    amountTaskDone: 50,
    done: false,
    members: TeamDate,
  },
];
