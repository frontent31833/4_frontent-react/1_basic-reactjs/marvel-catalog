import { ReactNode } from "react";

interface TasckcontentModel {
  content: string;
  finish: boolean;
}
export interface CheckListModel {
  title: string;
  icon: string | ReactNode;
  tascks: TasckcontentModel[];
}

export const ChecklistData: CheckListModel[] = [
  {
    title: '"Tasks Quick List"',
    icon: "AssignmentTwoToneIcon",
    tascks: [
      {
        content: "Prepare website launch",
        finish: false,
      },
      {
        content: "Build React Native application",
        finish: false,
      },
      {
        content: "Fix remaining bugs for first 4 pages",
        finish: false,
      },
    ],
  },
  {
    title: '"Project Management"',
    icon: "AccountTreeTwoToneIcon",
    tascks: [
      {
        content: "Complete sales pitch",
        finish: false,
      },
      {
        content: "Improve SEO visibility",
        finish: false,
      },
    ],
  },
  {
    title: '"Business & Marketing"',
    icon: "BusinessCenterTwoToneIcon",
    tascks: [
      {
        content: "Create marketing campaign",
        finish: false,
      },
      {
        content: "Sign business contract with partners",
        finish: false,
      },
    ],
  },
];
