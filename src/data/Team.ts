export interface Team {
  name: string;
  job: string;
  pathImg: string;
  completedTask: number;
  totalTask: number;
  status: string;
}

export const TeamDate: Team[] = [
  {
    name: "Hanna Siphron",
    job: "Web Dev Support Team",
    pathImg: "/PhotoReal_Black_Panther_avengers_2.jpg",
    completedTask: 4,
    totalTask: 6,
    status: "Offline",
  },
  {
    name: "Ann Saris",
    job: "Senior Book Keeper",
    pathImg: "/PhotoReal_black_widow_avengers_1.jpg",
    completedTask: 2,
    totalTask: 8,
    status: "Online",
  },
  {
    name: "Hanna Siphron",
    job: "Web Dev Support Team",
    pathImg:
      "/DreamShaper_v7_avengers_infinity_war_space_gem_in_dark_environ_0.jpg",
    completedTask: 10,
    totalTask: 20,
    status: "Offline",
  },
];
