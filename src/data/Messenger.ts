import { Team } from "./Team";
export interface MessengerModel {
  profile: Team;
  chats: chatsModel[];
}

interface chatsModel {
  porfile: Team;
  message: MessageModel;
  isRead: boolean;
  isArchive: boolean;
}

interface MessageModel {
  sender: string;
  content: string;
  createAt: Date;
}
