/* eslint-disable @typescript-eslint/no-unused-vars */
import { TasckData, TaskModel } from "../data/Task";
export interface TaskResponse {
  count: number;
  data: TaskModel[];
  from: number;
  to: number;
}
export const service = {
  getData: (from: number, to: number): Promise<TaskResponse> => {
    return new Promise<TaskResponse>((resolve, _reject) => {
      const data = TasckData.slice(from, to);
      resolve({
        count: TasckData.length,
        data: data,
        from: from,
        to: to,
      });
    });
  },
};
