import { Box, Divider, Drawer, IconButton, styled, useTheme } from '@mui/material'
import { useState } from 'react'
import ScrollBar from '../../../components/Scrollbar'
import SidebarContent from '../../../layout/Messenger/SidebarContent'
import MenuTwoToneIcon from '@mui/icons-material/MenuTwoTone';
import ChatContent from '../../../layout/Messenger/ChatContent';
import TopBarContent from '../../../layout/Messenger/TopBarContent';
import BottomBarContent from '../../../layout/Messenger/BottomBarContent';

const RootWraper = styled(Box)(
  ({ theme }) => `
    height: calc(100vh - ${theme.header.height});
    display:flex
  `
)
const Sidebar = styled(Box)(
  ({ theme }) => `
    width: 300px;
    background: ${theme.colors.alpha.white[100]};
    border-right: ${theme.colors.alpha.black[10]};
  `
)

const ChatWindow = styled(Box)(
  () => `
    width: 100%;
    height: 100%;
    display: flex;
    flex-direction: column;
    flex: 1;
  `
)

const ChatTopBar = styled(Box)(
  ({ theme }) => `
    background: ${theme.colors.alpha.white[100]};
    border-bottom: ${theme.colors.alpha.black[10]} solid 1px;
    padding: ${theme.spacing(2)};
    align-items: center;
  `
)

const IconButtonToggle = styled(IconButton)(
  ({ theme }) => `
    width: ${theme.spacing(4)};
    height: ${theme.spacing(4)};
    background: ${theme.colors.alpha.white[100]};
  `
)

const DrawerWrapperMobile = styled(Drawer)(
  () => `
    width: 340px;
    flex-shrink: 0;
    & > .MuiPaper-root {
      width: 340px;
      z-index: 3;
    }
  `
)
function Messenger() {
  const theme = useTheme()
  const [mobileOpen, setMobileOpen] = useState<boolean>(false)

  const handleMenuMobile = () => {
    setMobileOpen(!mobileOpen)
  }
  return (
    <RootWraper className='Mui-FixedWrapper'>
      <DrawerWrapperMobile
        sx={{
          display: { lg: 'none', xs: 'inline-block' }
        }}
        variant='temporary'
        anchor={theme.direction === 'rtl' ? 'right' : 'left'}
        open={mobileOpen}
        onClose={handleMenuMobile}
      >
        <ScrollBar>
          <SidebarContent />
        </ScrollBar>

      </DrawerWrapperMobile>
      <Sidebar
        sx={{
          display: { xs: 'none', lg: 'inline-block' }
        }}
      >
        <ScrollBar>
          <SidebarContent />
        </ScrollBar>
      </Sidebar>
      <Divider orientation="vertical" flexItem />
      <ChatWindow>
        <ChatTopBar
          sx={{
            display: { xs: 'flex', lg: 'inline-block' }
          }}
        >
          <IconButtonToggle
            sx={{
              display: { lg: 'none', xs: 'flex' },
              mr: 2
            }}
            color="primary"
            onClick={handleMenuMobile}
            size="small"
          >
            <MenuTwoToneIcon />

          </IconButtonToggle>
          <TopBarContent />
        </ChatTopBar>
        <Box
          flex={1}
        >
          <ScrollBar>
            <ChatContent />
          </ScrollBar>
        </Box>
        <Divider />
        <BottomBarContent />
      </ChatWindow>
    </RootWraper >
  )
}

export default Messenger