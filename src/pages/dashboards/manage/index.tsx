/* eslint-disable @typescript-eslint/ban-types */
// import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import { useState } from 'react';
import {
    Card,
    Grid,
    styled,
    Tabs,
    Tab,
    Divider
} from '@mui/material'
import { Container } from '@mui/system';
import { TasksAnalytics } from '../../../layout/ManagementTasks';
import { AnaliticsTask } from '../../../layout/Analitics';
import PageHeader from './PageHeader';

const TabsContainerWrapper = styled(Box)(
    ({ theme }) => `
        padding: 0 ${theme.spacing(2)};
        position: relative;
        bottom: -1px;
  
        .MuiTabs-root {
          height: 44px;
          min-height: 44px;
        }
  
        .MuiTabs-scrollableX {
          overflow-x: auto !important;
        }
  
        .MuiTabs-indicator {
            min-height: 4px;
            height: 4px;
            box-shadow: none;
            bottom: -4px;
            background: none;
            border: 0;
  
            &:after {
              position: absolute;
              left: 50%;
              width: 28px;
              content: ' ';
              margin-left: -14px;
              background: ${theme.colors.primary.main};
              border-radius: inherit;
              height: 100%;
            }
        }
  
        .MuiTab-root {
            &.MuiButtonBase-root {
                height: 44px;
                min-height: 44px;
                background: ${theme.colors.alpha.white[50]};
                border: 1px solid ${theme.colors.alpha.black[10]};
                border-bottom: 0;
                position: relative;
                margin-right: ${theme.spacing(1)};
                font-size: ${theme.typography.pxToRem(14)};
                color: ${theme.colors.alpha.black[70]};
                border-bottom-left-radius: 0;
                border-bottom-right-radius: 0;
  
                .MuiTouchRipple-root {
                  opacity: .1;
                }
  
                &:after {
                  position: absolute;
                  left: 0;
                  right: 0;
                  width: 100%;
                  bottom: 0;
                  height: 1px;
                  content: '';
                  background: ${theme.colors.alpha.black[10]};
                }
  
                &:hover {
                  color: ${theme.colors.alpha.black[100]};
                }
            }
  
            &.Mui-selected {
                color: ${theme.colors.alpha.black[100]};
                background: ${theme.colors.alpha.white[100]};
                border-bottom-color: ${theme.colors.alpha.white[100]};
  
                &:after {
                  height: 0;
                }
            }
        }
    `
);

export function Manage() {
    const [currentTab, setCurrentTab] = useState<string>('analytics');


    const tabs = [
        { value: 'analytics', label: 'Analytics Overview' },
        { value: 'taskSearch', label: 'Task Search' }
    ];
    const handleChange = (_event: React.ChangeEvent<{}>, value: string) => {
        setCurrentTab(value);
    };

    return (
        <Container maxWidth='lg'>
            <PageHeader />
            <Divider sx={{
                my: 4
            }} />
            <TabsContainerWrapper>
                <Tabs
                    onChange={handleChange}
                    value={currentTab}
                    variant="scrollable"
                    scrollButtons="auto"
                    textColor="primary"
                    indicatorColor="primary"
                >
                    {tabs.map((tab) => (
                        <Tab key={tab.value} label={tab.label} value={tab.value} />
                    ))}
                </Tabs>
            </TabsContainerWrapper>
            <Card variant='outlined'>
                <Grid
                    container
                    direction="row"
                    justifyContent="center"
                    alignItems="stretch"
                    spacing={0}
                    p={4}
                >
                    {currentTab === 'analytics' && (
                        <TasksAnalytics />
                    )}
                    {currentTab === 'taskSearch' && (
                        <AnaliticsTask />
                    )}
                </Grid>
            </Card>
        </Container>
    )
}