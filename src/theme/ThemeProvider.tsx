import { ReactNode, useContext } from 'react'
import { ThemeProvider } from '@mui/material'
import { ThemeContext } from '../Context/ThemeContext'
import { StylesProvider } from '@mui/styles';




function AppThemeProvider({ children }: { children: ReactNode }) {
    const { themeScheme, themeCreator } = useContext(ThemeContext)
    // const contextValue = { setThemeScheme }
    return (
        <StylesProvider injectFirst>
            <ThemeContext.Provider value={{ themeCreator }}>
                <ThemeProvider theme={themeScheme}>
                    {children}
                </ThemeProvider>
            </ThemeContext.Provider>
        </StylesProvider>
    )
}

export default AppThemeProvider

