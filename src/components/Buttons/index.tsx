import IconButton from '@mui/material/IconButton';
// import DeleteIcon from '@mui/icons-material/Delete';
import DehazeIcon from '@mui/icons-material/Dehaze';

function Button() {
    return (
        <IconButton sx={{
            borderRadius: '0',
            border: '2px solid #6c6c6c'
        }} aria-label="delete" color='primary' size="large">
            <DehazeIcon fontSize="inherit" />
        </IconButton>
    )
}

export default Button