import { Box, useTheme } from "@mui/material";
import { ReactNode } from "react";
import PropTypes from 'prop-types';
import { Scrollbars } from 'react-custom-scrollbars-2'

interface ScrollbarProps {
    className?: string;
    children?: ReactNode;
}
function ScrollBar({ children, ...rest }: ScrollbarProps) {
    const theme = useTheme()
    return (
        <Scrollbars
            autoHide
            universal
            renderThumbVertical={() => {
                return (
                    <Box
                        sx={{
                            width: 5,
                            background: `${theme.colors.alpha.black[10]}`,
                            borderRadius: `${theme.general.borderRadiusLg}`,
                            transition: `${theme.transitions.create(['background'])}`,

                            '&:hover': {
                                background: `${theme.colors.alpha.black[30]}`
                            }
                        }}
                    />
                )
            }}
            {...rest}
        // className={className}
        >
            {children}
        </Scrollbars>
    )
}

ScrollBar.propTypes = {
    children: PropTypes.node,
    className: PropTypes.string
};

export default ScrollBar