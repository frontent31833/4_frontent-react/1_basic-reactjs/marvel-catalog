import { Box, Grid, Typography, Avatar, Badge, Tooltip, useTheme, LinearProgress, styled } from '@mui/material'
import { Team } from '../../../data/Team'
import { formatDistance, subDays } from 'date-fns'
import { Text } from '../../Text'
const DotLegend = styled('span')(
    ({ theme }) => `
        border-radius: 22px;
        width: ${theme.spacing(1.5)};
        height: ${theme.spacing(1.5)};
        display: inline-block;
        margin-right: ${theme.spacing(0.5)};
        border: ${theme.colors.alpha.white[100]} solid 2px;
        animation: pulse 2s cubic-bezier(0.4, 0, 0.6, 1) infinite;

        @keyframes pulse {
            0%, 100% {
                opacity: 1;
            }
            50% {
                opacity: .5;
            }
        }
  `
);
const AvatarWrapper = styled(Avatar)(
    ({ theme }) => `
        width: ${theme.spacing(15)};
        height: ${theme.spacing(15)};
        
        .MuiAvatar-img{ 
            transition: all 700ms ease-in-out;
        }
        .MuiAvatar-img:hover{ 
            transform: scale(2);
            transition: all 700ms ease-in-out;
        }
  `
);

export function TeamCard({ name, job, pathImg, completedTask, totalTask, status }: Team) {
    const theme = useTheme()
    return (
        <Grid item xs={12} md={4} p={3}>
            <Box>
                <Box display='flex' alignItems='center' justifyContent='space-evenly' pb={3}>
                    <Badge
                        anchorOrigin={{
                            vertical: 'bottom',
                            horizontal: 'right'
                        }}
                        overlap='circular'
                        badgeContent={
                            <Tooltip
                                arrow
                                placement='top'
                                title={
                                    'Offline since ' + formatDistance(subDays(new Date(), 14), new Date(), {
                                        addSuffix: true
                                    })
                                }
                            >
                                <DotLegend style={{
                                    background: `${status === 'Offline' ? theme.colors.error.main : theme.colors.success.main}`,
                                    width: '15px',
                                    height: '15px'
                                }} />
                            </Tooltip>
                        }
                    >
                        <AvatarWrapper src={pathImg} alt={name} />
                    </Badge>
                    <Box sx={{
                        ml: 1.5
                    }}>
                        <Typography variant='h4' noWrap gutterBottom>
                            {name}
                        </Typography>
                        <Typography variant='subtitle2' noWrap>
                            {job}
                        </Typography>
                    </Box>
                </Box>
                <Typography variant='subtitle2' gutterBottom>
                    <Text color='black'>{completedTask}</Text> out of <Text color='black'>{totalTask}</Text> {' '} tascks completedTask
                </Typography>
                <LinearProgress
                    value={completedTask}
                    color='primary'
                    variant='determinate'
                    sx={{ height: '10px' }}
                />
            </Box>
        </Grid>
    )
}

