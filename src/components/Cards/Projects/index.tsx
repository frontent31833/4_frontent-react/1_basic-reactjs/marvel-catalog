import {
    Grid,
    Box,
    CardHeader,
    Avatar,
    styled,
    useTheme,
    Typography,
    LinearProgress,
    AvatarGroup,
    Tooltip,
    Link,
    IconButton
} from '@mui/material'
import CheckTwoToneIcon from '@mui/icons-material/CheckTwoTone';
import { Project } from '../../../data/Project'
import { Text } from '../../Text';
import { Team } from '../../../data/Team';
import { v4 as uuid } from 'uuid'
import MoreVertTwoToneIcon from '@mui/icons-material/MoreVertTwoTone';
import CalendarTodayTwoToneIcon from '@mui/icons-material/CalendarTodayTwoTone';
import StarTwoToneIcon from '@mui/icons-material/StarTwoTone';

const AvatarWrapperSuccess = styled(Avatar)(
    ({ theme }) => `
        background-color: ${theme.colors.success.lighter};
        color:  ${theme.colors.success.main};
    `
)
const AvatarWrapperPending = styled(Avatar)(
    ({ theme }) => `
        background-color: ${theme.colors.warning.lighter};
        color:  ${theme.colors.warning.main};
    `
)
const LinearProgressWrapper = styled(LinearProgress)(
    ({ theme }) => `
        flex-grow:1;
        height: 10px;
        &.MuiLinearProgress-root {
            background-color: ${theme.colors.alpha.black[10]};
        }
          
        .MuiLinearProgress-bar {
            border-radius: ${theme.general.borderRadiusXl};
        }
    `
)
export function Projects({ task, amountTaskDone, members, done }: Project) {
    const theme = useTheme()
    return (
        <Grid item xs={12} md={4} >
            <Box>
                <CardHeader
                    sx={{
                        px: 0,
                        pt: 0
                    }}
                    avatar={done ?
                        <AvatarWrapperSuccess>
                            <CheckTwoToneIcon />
                        </AvatarWrapperSuccess>
                        :
                        <AvatarWrapperPending>
                            {task.split(' ')[0][0]}{task.split(' ')[1][0]}
                        </AvatarWrapperPending>
                    }
                    action={
                        <IconButton size="small" color="primary">
                            <MoreVertTwoToneIcon />
                        </IconButton>
                    }
                    title={task}
                    titleTypographyProps={{
                        variant: 'h5',
                        color: 'textPrimary'
                    }}
                />

                <Box>
                    <Typography display='flex' variant='subtitle2' gutterBottom>
                        Task done:{' '}
                        <Text color="black">
                            <b>{amountTaskDone}</b>
                        </Text>
                        <b>/100</b>
                    </Typography>
                    <LinearProgressWrapper value={amountTaskDone} color='primary' variant='determinate' />
                </Box>
                <Box
                    sx={{
                        display: 'flex',
                        alignItems: 'center',
                        justifyContent: 'space-between',
                        pt: 2
                    }}
                >
                    <AvatarGroup>
                        {members.map((member: Team) => (
                            <Tooltip arrow title={`View profile ${member.name}`} key={uuid()}>
                                <Avatar
                                    sx={{
                                        width: 30,
                                        height: 30,
                                    }}
                                    component={Link}
                                    href='#'
                                    alt={`${member.name}`}
                                    src={member.pathImg}
                                />
                            </Tooltip>
                        ))}
                    </AvatarGroup>
                    <Box>
                        <Tooltip
                            arrow
                            title='View project in calendar'
                            placement='top'
                        >
                            <IconButton
                                size='small'
                                color='secondary'
                                sx={{
                                    ml: 0.5
                                }}
                            >
                                <CalendarTodayTwoToneIcon fontSize='small' />
                            </IconButton>

                        </Tooltip>
                        <Tooltip
                            arrow
                            title='View project in calendar'
                            placement='top'
                        >
                            <IconButton
                                size="small"
                                sx={{
                                    color: `${theme.colors.warning.main}`,
                                    ml: 0.5
                                }}
                            >
                                <StarTwoToneIcon fontSize='small' />
                            </IconButton>

                        </Tooltip>
                    </Box>
                </Box>
            </Box>
        </Grid>
    )
}