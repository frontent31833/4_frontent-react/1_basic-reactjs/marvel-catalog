import { Card, Chip, Grid, Link, Button, Rating, Typography, useTheme, Box, Divider, CardActions, AvatarGroup, Avatar, Tooltip } from '@mui/material'
import { TaskModel } from '../../../data/Task'
import { formatDistance, subDays } from 'date-fns';
import ArrowForwardIcon from '@mui/icons-material/ArrowForward';
import TodayTwoToneIcon from '@mui/icons-material/TodayTwoTone';
import { v4 as uuid } from 'uuid'
function Task({ rating, title, categori, body, members, totalTime }: TaskModel) {
    const theme = useTheme()
    function handleClick() {

    }
    function handleDelete() {

    }
    return (
        <Grid item xs={12} md={4}>
            <Card
                variant='outlined'
                sx={{
                    p: 3,
                    background: `${theme.colors.alpha.black[5]}`,
                    textAlign: 'center'
                }}
            >
                <Box>
                    <Rating value={rating} defaultValue={0} precision={1} readOnly />
                </Box>
                <Link component={'a'} href='#' variant='h3' color="text.primary" >
                    {title}
                </Link>
                <Box sx={{
                    py: 2
                }}>
                    {categori.map((cat) => (
                        <Chip
                            key={uuid()}
                            sx={{
                                mr: 0.5
                            }}
                            size='small'
                            label={cat.title}
                            color='secondary'
                            onClick={handleClick}
                            onDelete={handleDelete}
                        />
                    ))}
                </Box>
                <Typography
                    sx={{
                        pb: 2
                    }}
                    color="text.secondary"
                >
                    {body}
                </Typography>
                <Button size='small' variant='contained' endIcon={<ArrowForwardIcon />}>
                    Ver tarea
                </Button>
                <Divider
                    sx={{
                        my: 2
                    }}
                />
                <CardActions
                    sx={{
                        display: 'flex',
                        alignItems: 'center',
                        justifyContent: 'space-between'
                    }}
                >
                    <Typography
                        display='flex'
                        alignItems='center'
                        variant='subtitle2'
                    >
                        <TodayTwoToneIcon
                            sx={{
                                mr: 1
                            }}
                        />
                        {formatDistance(subDays(new Date(), totalTime), new Date(), {
                            addSuffix: true
                        })}
                    </Typography>
                    <AvatarGroup>
                        {members.map((member) => (
                            <Tooltip
                                arrow
                                title={`view profile for ${member.name}`}
                                key={uuid()}
                            >
                                <Avatar
                                    sx={{
                                        width: 35,
                                        height: 35
                                    }}
                                    component={Link}
                                    href='#'
                                    alt={member.name}
                                    src={member.pathImg}
                                />
                            </Tooltip>
                        ))}
                    </AvatarGroup>
                </CardActions>
            </Card>
        </Grid>
    )
}

export default Task