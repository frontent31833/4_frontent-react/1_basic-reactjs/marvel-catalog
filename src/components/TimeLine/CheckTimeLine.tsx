import { Checkbox, FormControlLabel, FormGroup, Typography, styled } from "@mui/material"

import TimelineItem from "@mui/lab/TimelineItem";
import TimelineSeparator from "@mui/lab/TimelineSeparator";
import TimelineDot from "@mui/lab/TimelineDot";
import TimelineConnector from "@mui/lab/TimelineConnector";
import TimelineContent from "@mui/lab/TimelineContent";
import { CheckListModel } from '../../data/CheckList'
import { useState } from "react";
import { v4 as uuid } from 'uuid'


const CheckboxWrapper = styled(Checkbox)(
    ({ theme }) => `
      padding: ${theme.spacing(0.5)};
  `
);

function CheckTimeLine({ title, icon, tascks }: CheckListModel) {
    const [checked, setChecked] = useState(false)
    function hamdleChecket(event: React.ChangeEvent<HTMLInputElement>) {
        if (event.target.checked)
            setChecked(!checked)
        else
            setChecked(!checked)
    }
    return (

        <TimelineItem>
            <TimelineSeparator>
                <TimelineDot color="primary">
                    {icon}
                </TimelineDot>
                <TimelineConnector />
            </TimelineSeparator>
            <TimelineContent>
                <Typography
                    variant="h4"
                    sx={{
                        pb: 2
                    }}
                >
                    {title}
                </Typography>
                <FormGroup>
                    {tascks.map((tasck) => (
                        <FormControlLabel
                            control={<CheckboxWrapper checked={checked} onChange={hamdleChecket} color='primary' name="checkedC" />}
                            label={tasck.content}
                            key={uuid()}
                        />
                    ))}
                </FormGroup>
            </TimelineContent>
        </TimelineItem>
    )
}

export default CheckTimeLine