import { Box, styled } from '@mui/material';
import './App.css'
import { SidebarLayout } from './layout/SidebarLayout';
import { Outlet } from "react-router-dom";
const OverviewWrapper = styled(Box)(
  ({ theme }) => `
    overflow: auto;
    background: ${theme.palette.common.white};
    flex: 1;
    overflow-x: hidden;
`
);

function App() {


  return (
    <OverviewWrapper>
      <SidebarLayout>
        <Outlet />
      </SidebarLayout>
    </OverviewWrapper>
  )
}

export default App
