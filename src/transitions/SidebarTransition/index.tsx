/* eslint-disable @typescript-eslint/no-explicit-any */
import { Slide } from '@mui/material'
import { TransitionProps } from '@mui/material/transitions'
import { ReactElement, Ref, forwardRef } from 'react'

export const Trasition = forwardRef(function transition(
    props: TransitionProps & { children: ReactElement<any, any> },
    ref: Ref<unknown>
) {
    return <Slide direction='down' ref={ref} {...props} />
})