/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable @typescript-eslint/no-explicit-any */
import { FormControl, InputAdornment, OutlinedInput, styled, Button, Box, Typography, Menu, MenuItem, Grid, Pagination } from '@mui/material'
import SearchTwoToneIcon from '@mui/icons-material/SearchTwoTone';
import { Text } from '../../components/Text';
import { useEffect, useRef, useState } from 'react'
import { v4 as uuid } from 'uuid'

import { TaskModel } from '../../data/Task'

import ExpandMoreTwoToneIcon from '@mui/icons-material/ExpandMoreTwoTone';
import Task from '../../components/Cards/Task';

import { service, TaskResponse } from '../../utils/paginationCount'
const OutlinedInputWrapper = styled(OutlinedInput)(
    ({ theme }) => `
        background-color: ${theme.colors.alpha.white[100]};
        padding-right: ${theme.spacing(0.7)}
    `
)
interface Peridods {
    value: string,
    text: string
}
const pageSize = 3
export function AnaliticsTask() {
    const periods: Peridods[] = [
        {
            value: 'popular',
            text: 'Most popular'
        },
        {
            value: 'recent',
            text: 'Recent tasks'
        },
        {
            value: 'updated',
            text: 'Latest updated tasks'
        },
        {
            value: 'oldest',
            text: 'Oldest tasks first'
        }
    ];
    const menuRef = useRef<any>(null)
    const [period, setPeriod] = useState<string>(periods[0].value)
    const [viewMenu, setViewMenu] = useState<boolean>(false)

    function handleMenuItem(periodItem: Peridods) {
        setPeriod(periodItem.value)
        setViewMenu(!viewMenu)

    }
    const [tasks, setTasks] = useState<TaskModel[]>([])
    const [pagination, setPagination] = useState<TaskResponse>({
        count: 0,
        data: [],
        from: 0,
        to: pageSize
    })
    useEffect(() => {
        service.getData(pagination.from, pagination.to).then((res: TaskResponse) => {
            setPagination({ ...pagination, count: res.count })
            setTasks(res.data)
        })
    }, [pagination.from, pagination.to])

    function handlePaginationchange(_event: any, page: number) {
        const from = (page - 1) * pageSize;
        const to = (page - 1) * pageSize + pageSize;
        setPagination({ ...pagination, from: from, to: to })
    }
    return (
        <>
            <FormControl sx={{
                width: '100%'
            }}>
                <OutlinedInputWrapper
                    type='text'
                    placeholder='Search terms here...'
                    endAdornment={
                        <InputAdornment position='end'>
                            <Button variant='contained' size='small'>
                                Buscar
                            </Button>
                        </InputAdornment>
                    }
                    startAdornment={
                        <InputAdornment position='start'>
                            <SearchTwoToneIcon />
                        </InputAdornment>
                    }
                />
            </FormControl>
            <Box
                py={3}
                display="flex"
                alignItems="center"
                justifyContent="space-between"
                sx={{ width: '100%' }}
            >
                <Box>
                    <Typography variant='subtitle2'>
                        Showing{' '}
                        <Text color='black'>
                            <b>57 tascks</b>
                        </Text>
                    </Typography>
                </Box>
                <Box display='flex' alignItems='center'>
                    <Typography variant='subtitle2' sx={{ pr: 1 }}>
                        Sort by:
                    </Typography>
                    <Button
                        ref={menuRef}
                        variant='outlined' color='primary' size='small'
                        onClick={() => { setViewMenu(true) }}
                        endIcon={<ExpandMoreTwoToneIcon fontSize='small' />}
                    >
                        {period}
                    </Button>
                    <Menu
                        disableScrollLock
                        anchorEl={menuRef.current}
                        onClose={() => { setViewMenu(false) }}
                        open={viewMenu}
                        anchorOrigin={{
                            vertical: 'bottom',
                            horizontal: 'center'
                        }}
                        transformOrigin={{
                            vertical: 'top',
                            horizontal: 'center'
                        }}
                    >
                        {periods.map((periodItem) => (
                            <MenuItem
                                key={uuid()}
                                onClick={() => { handleMenuItem(periodItem) }}
                            >
                                {periodItem.value}
                            </MenuItem>
                        ))}
                    </Menu>
                </Box>
            </Box>
            <Grid
                container
                spacing={3}
            >
                {tasks.map((data: TaskModel) => (
                    <Task body={data.body} categori={data.categori} members={data.members} rating={data.rating} title={data.title} totalTime={data.totalTime} key={uuid()} />
                ))}
            </Grid>
            <Box
                sx={{
                    pt: 4
                }}
                display='flex'
                alignItems="center"
                justifyContent="center"

            >
                <Pagination
                    showFirstButton
                    showLastButton
                    count={Math.ceil(pagination.count / pageSize)}
                    onChange={(event, page) => { handlePaginationchange(event, page) }}
                    // defaultPage={6}
                    siblingCount={0}
                    size="large"
                    shape="rounded"
                    color="primary"
                />
            </Box>
        </>
    )
}
