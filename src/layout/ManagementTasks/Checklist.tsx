import { Box, CardHeader, IconButton, Tooltip, styled } from "@mui/material"
import RefreshTwoToneIcon from '@mui/icons-material/RefreshTwoTone';
import AssignmentTwoToneIcon from '@mui/icons-material/AssignmentTwoTone';
import AccountTreeTwoToneIcon from '@mui/icons-material/AccountTreeTwoTone';
import BusinessCenterTwoToneIcon from '@mui/icons-material/BusinessCenterTwoTone';
import { ReactNode } from "react";
import { CheckListModel, ChecklistData } from '../../data/CheckList'
import CheckTimeLine from "../../components/TimeLine/CheckTimeLine";
import { v4 as uuid } from 'uuid'
import Timeline from "@mui/lab/Timeline";

const TimelineWrapper = styled(Timeline)(
    ({ theme }) => `
      margin-left: ${theme.spacing(2)};
  
      .MuiTimelineDot-root {
        left: -${theme.spacing(2)};
        margin-top: 0;
        top: ${theme.spacing(0.5)};
      }
      
      .MuiTimelineContent-root {
        padding-left: ${theme.spacing(4)};
      }
      
      .MuiFormControlLabel-root {
        margin-left: -${theme.spacing(0.7)};
      }
      
      .MuiFormControlLabel-label {
        color: ${theme.colors.alpha.black[50]};
      }
  `
);

const iconMap = (key: string | ReactNode): ReactNode => {
    switch (key) {
        case 'AssignmentTwoToneIcon': {
            return < AssignmentTwoToneIcon />
        }
        case 'AccountTreeTwoToneIcon': {
            return <AccountTreeTwoToneIcon />
        }
        case 'BusinessCenterTwoToneIcon': {
            return <BusinessCenterTwoToneIcon />
        }
    }
}


function Checklist() {

    return (
        <Box
        >
            <CardHeader
                sx={{
                    px: 0,
                    pt: 0,
                }}
                action={
                    <Tooltip arrow title='refresh list'>
                        <IconButton size="small" color="primary">
                            <RefreshTwoToneIcon />
                        </IconButton>
                    </Tooltip>
                }
                title='CheckList'
            />
            <TimelineWrapper>

                {ChecklistData.map((data: CheckListModel) => (
                    <CheckTimeLine tascks={data.tascks} title={data.title} icon={iconMap(data.icon)} key={uuid()} />
                ))}
            </TimelineWrapper>
        </Box>
    )
}

export default Checklist