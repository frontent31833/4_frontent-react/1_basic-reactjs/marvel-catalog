import { Grid } from '@mui/material'
import { Team, TeamDate } from '../../data/Team'
import { TeamCard } from '../../components/Cards/Team'
import { v4 as uuid } from 'uuid'
export function TeamOverview() {
    return (
        <Grid container spacing={4}>
            {TeamDate.map((team: Team) => (
                <TeamCard name={team.name} job={team.job} pathImg={team.pathImg} completedTask={team.completedTask} totalTask={team.totalTask} key={uuid()} status={team.status} />
            ))}
        </Grid>
    )
}
