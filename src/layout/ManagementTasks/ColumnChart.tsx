import {
    useTheme,
    alpha
} from '@mui/material'
import { Chart } from '../../components/Chart'
import type { ApexOptions } from 'apexcharts';
export function ColumnChart() {
    const theme = useTheme()
    const chartOptions: ApexOptions = {
        chart: {
            background: 'transparent',
            type: 'bar',
            toolbar: {
                show: false
            },
            zoom: {
                enabled: true
            }
        },
        plotOptions: {
            bar: {
                horizontal: false,
                borderRadius: 6,
                columnWidth: '35%'
            }
        },
        colors: [theme.colors.primary.main, alpha(theme.colors.primary.main, 0.5)],
        dataLabels: {
            enabled: false
        },
        fill: {
            opacity: 1
        },
        theme: {
            mode: theme.palette.mode
        },
        stroke: {
            show: true,
            width: 3,
            colors: ['transparent']
        },
        legend: {
            show: false
        },
        labels: [
            'Jan',
            'Feb',
            'Mar',
            'Apr',
            'May',
            'Jun',
            'Jul',
            'Aug',
            'Sep',
            'Oct',
            'Nov',
            'Dec'
        ],
        grid: {
            strokeDashArray: 5,
            borderColor: theme.palette.divider
        },
        xaxis: {
            axisBorder: {
                show: false
            },
            axisTicks: {
                show: false
            },
            labels: {
                style: {
                    colors: theme.palette.text.secondary
                }
            }
        },
        yaxis: {
            tickAmount: 6,
            axisBorder: {
                show: false
            },
            axisTicks: {
                show: false
            },
            labels: {
                style: {
                    colors: theme.palette.text.secondary
                }
            }
        },
        tooltip: {
            x: {
                show: false
            },
            marker: {
                show: false
            },
            y: {
                formatter: function (val) {
                    return '$ ' + val + 'k';
                }
            },
            theme: 'dark'
        }
    };

    const chartData = [
        {
            name: 'Income',
            data: [28, 47, 41, 34, 69, 91, 49, 82, 52, 72, 32, 99]
        },
        {
            name: 'Expenses',
            data: [38, 85, 64, 40, 97, 82, 58, 42, 55, 46, 57, 70]
        }
    ];

    return (
        <>
            <Chart
                options={chartOptions}
                series={chartData}
                type="bar"
                height={270}
            // width={270}
            />
        </>
    )
}
