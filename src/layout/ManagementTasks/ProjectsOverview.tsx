
import { Project, ProjectData } from '../../data/Project'
import { Projects } from '../../components/Cards/Projects'
import { v4 as uuid } from 'uuid'
import { Grid } from '@mui/material'
export function ProjectsOverview() {
    return (
        <Grid container spacing={4}>
            {ProjectData.map((project: Project) => (
                <Projects amountTaskDone={project.amountTaskDone} done={project.done} members={project.members} task={project.task} key={uuid()} />
            ))}
        </Grid>
    )
}