/* eslint-disable @typescript-eslint/no-unused-vars */
import { Avatar, Box, CardActions, Collapse, Divider, List, ListItem, ListItemText, Rating, Tooltip, Typography, useTheme, styled } from '@mui/material'
import { useState } from 'react'
import PhoneTwoToneIcon from '@mui/icons-material/PhoneTwoTone';
import EmailTwoToneIcon from '@mui/icons-material/EmailTwoTone';
import MessageTwoToneIcon from '@mui/icons-material/MessageTwoTone';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import IconButton, { IconButtonProps } from '@mui/material/IconButton';

interface ExpandMoreProps extends IconButtonProps {
    expand: boolean;
}

const ExpandMore = styled((props: ExpandMoreProps) => {
    const { expand, ...other } = props;
    return <IconButton {...other} />;
})(({ theme, expand }) => ({
    transform: !expand ? 'rotate(0deg)' : 'rotate(180deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
        duration: theme.transitions.duration.shortest,
    }),
}));

function Profile() {
    const theme = useTheme()
    const [ratingValue, setRatingValue] = useState<number | null>(0)
    function handleRating(value: number | null) {
        setRatingValue(value)
    }
    const [expanded, setExpanded] = useState(false);

    const handleExpandClick = () => {
        setExpanded(!expanded);
    };
    return (
        <Box
            sx={{
                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'center',
                alignItems: 'center',
                gap: 2
            }}
        >
            <Avatar
                sx={{
                    mb: 1.5,
                    width: theme.spacing(25),
                    height: theme.spacing(25),
                    mx: 'auto',
                }}
                variant='rounded'
                alt='useR Profile'
                src='alchemyrefiner_alchemymagic_1_25460ca9-b43b-48f8-a9ac-a8be7a76cde8_0.jpg'
            />
            <Box>
                <Typography align="center" variant="h4" gutterBottom>
                    Craig Donin
                </Typography>
                <Typography align="center" variant="subtitle2" gutterBottom>
                    Senior Web Developer
                </Typography>
            </Box>
            <Box display="flex" alignItems="center" justifyContent="center" gap={2}>
                <Rating name="half-rating" value={ratingValue} defaultValue={0} precision={0.5} onChange={(_event, newValue) => { handleRating(newValue) }} />
                <Typography
                    variant="h5"
                    sx={{
                        pl: 0.5
                    }}
                >
                    {ratingValue}
                </Typography>
            </Box>
            <CardActions disableSpacing>
                <ExpandMore
                    expand={expanded}
                    onClick={handleExpandClick}
                    aria-expanded={expanded}
                    aria-label="show more"
                >
                    <ExpandMoreIcon />
                </ExpandMore>
            </CardActions>
            <Collapse
                sx={{ width: '100%', maxWidth: 360 }}
                in={expanded} timeout="auto" unmountOnExit
            >
                <Box
                    sx={{
                        display: 'flex',
                        justifyContent: 'center',
                        alignItems: 'center',
                        gap: 3,
                    }}
                >
                    <Tooltip arrow placement="top" title="Call">
                        <IconButton
                            color="primary"
                            sx={{
                                mx: 0.5
                            }}
                        >
                            <PhoneTwoToneIcon />
                        </IconButton>
                    </Tooltip>
                    <Tooltip arrow placement="top" title="Send email">
                        <IconButton
                            color="primary"
                            sx={{
                                mx: 0.5
                            }}
                        >
                            <EmailTwoToneIcon />
                        </IconButton>
                    </Tooltip>
                    <Tooltip arrow placement="top" title="Send Message">
                        <IconButton
                            color="primary"
                            sx={{
                                mx: 0.5
                            }}
                        >
                            <MessageTwoToneIcon />
                        </IconButton>
                    </Tooltip>
                </Box>
                <List
                    sx={{ width: '100%', maxWidth: 360 }}
                >
                    <Divider component='li' />
                    <ListItem
                        sx={{
                            pt: 2
                        }}
                    >
                        <ListItemText
                            primary="Join Date"
                            primaryTypographyProps={{ variant: 'subtitle2' }}
                        />
                        <Typography>
                            22 January 2021
                        </Typography>
                    </ListItem>
                    <Divider component='li' />
                    <ListItem
                        sx={{
                            pt: 2
                        }}
                    >
                        <ListItemText
                            primary="Company"
                            primaryTypographyProps={{ variant: 'subtitle2' }}
                        />
                        <Typography>
                            Google Inc
                        </Typography>
                    </ListItem>
                    <Divider component='li' />
                    <ListItem
                        sx={{
                            pt: 2
                        }}
                    >
                        <ListItemText
                            primary="Tasks"
                            primaryTypographyProps={{ variant: 'subtitle2' }}
                        />
                        <Typography>
                            67 active
                        </Typography>
                    </ListItem>
                </List>
            </Collapse>
        </Box >
    )
}

export default Profile