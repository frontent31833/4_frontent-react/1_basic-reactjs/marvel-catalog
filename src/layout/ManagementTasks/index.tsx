/* eslint-disable @typescript-eslint/no-explicit-any */
import {
    Button,
    Box,
    Menu,
    MenuItem,
    Typography,
    styled,
    Grid,
    Divider,
    useTheme,
} from '@mui/material';
import { ColumnChart } from './ColumnChart'
import { useRef, useState } from 'react'
import ExpandMoreTwoToneIcon from '@mui/icons-material/ExpandMoreTwoTone';
import { Performance } from './Performance'
import { TeamOverview } from './TeamOverview';
import { ProjectsOverview } from './ProjectsOverview';
import Checklist from './Checklist';
import Profile from './Profile';
import { Container } from '@mui/system';
const DotPrimaryLight = styled('span')(
    ({ theme }) => `
      border-radius: 22px;
      background: ${theme.colors.primary.lighter};
      width: ${theme.spacing(1.5)};
      height: ${theme.spacing(1.5)};
      display: inline-block;
      margin-right: ${theme.spacing(0.5)};
  `
);

const DotPrimary = styled('span')(
    ({ theme }) => `
      border-radius: 22px;
      background: ${theme.colors.primary.main};
      width: ${theme.spacing(1.5)};
      height: ${theme.spacing(1.5)};
      display: inline-block;
      margin-right: ${theme.spacing(0.5)};
  `
);
function TasksAnalytics() {
    const theme = useTheme()
    const periods = [
        {
            value: 'today',
            text: 'Today'
        },
        {
            value: 'yesterday',
            text: 'Yesterday'
        },
        {
            value: 'last_month',
            text: 'Last month'
        },
        {
            value: 'last_year',
            text: 'Last year'
        }
    ];
    const actionRef1 = useRef<any>(null);
    const [openPeriod, setOpenMenuPeriod] = useState<boolean>(false);
    const [period, setPeriod] = useState<string>(periods[3].text);
    return (
        <Container maxWidth='xl' >
            <Grid item xs={12} py={4}>
                <Box>
                    <TeamOverview />
                </Box>
            </Grid>
            <Divider variant='fullWidth' />
            <Grid container spacing={4} py={4}>
                <Grid item xs={12} sm={6} md={8}>
                    <Box
                        mb={2}
                        display="flex"
                        alignItems="center"
                        justifyContent="space-between"
                    >
                        <Typography variant="h4">Tasks Analytics</Typography>
                        <Button
                            size="small"
                            variant="contained"
                            color="secondary"
                            ref={actionRef1}
                            onClick={() => setOpenMenuPeriod(true)}
                            endIcon={<ExpandMoreTwoToneIcon fontSize="small" />}
                        >
                            {period}
                        </Button>
                        <Menu
                            disableScrollLock
                            anchorEl={actionRef1.current}
                            onClose={() => setOpenMenuPeriod(false)}
                            open={openPeriod}
                            anchorOrigin={{
                                vertical: 'bottom',
                                horizontal: 'right'
                            }}
                            transformOrigin={{
                                vertical: 'top',
                                horizontal: 'right'
                            }}
                        >
                            {periods.map((_period) => (
                                <MenuItem
                                    key={_period.value}
                                    onClick={() => {
                                        setPeriod(_period.text);
                                        setOpenMenuPeriod(false);
                                    }}
                                >
                                    {_period.text}
                                </MenuItem>
                            ))}
                        </Menu>
                    </Box>
                    <Box display="grid" alignItems="center" pb={2}>
                        <Typography
                            variant="body2"
                            color="text.secondary"
                            sx={{
                                display: 'flex',
                                alignItems: 'center',
                                mr: 2
                            }}
                        >
                            <DotPrimary />
                            tasks created
                        </Typography>
                        <Typography
                            variant="body2"
                            color="text.secondary"
                            sx={{
                                display: 'flex',
                                alignItems: 'center'
                            }}
                        >
                            <DotPrimaryLight />
                            tasks completed
                        </Typography>
                    </Box>
                    <ColumnChart />
                </Grid>
                <Grid item xs={12} sm={6} md={4}>
                    <Performance />
                </Grid>
            </Grid>
            <Divider variant='fullWidth' />
            <Grid container spacing={4} py={4}>
                <Grid item xs={12} sm={12} md={12}>
                    <Box
                        mb={2}
                        display="flex"
                        alignItems="center"
                        justifyContent="space-between"
                    >

                        <Typography variant='h3'>Proyects</Typography>
                        <Button size='small' variant='outlined'>View All Proyects</Button>
                    </Box>
                </Grid>
                <Grid item xs={12} sm={12} md={12}>
                    <Box
                        mb={2}
                        display="flex"
                        alignItems="center"
                        justifyContent="space-between"
                    >

                        <ProjectsOverview />
                    </Box>
                </Grid>
            </Grid>
            <Divider variant='fullWidth' />
            <Grid container my={4} >


                <Grid item xs={12} sm={6} md={6}>
                    <Box
                        p={4}
                        sx={{
                            background: `${theme.colors.alpha.white[70]}`
                        }}>
                        <Checklist />
                    </Box>
                </Grid>
                <Grid item xs={12} sm={6} md={6}>
                    <Box
                        p={4}
                        sx={{
                            background: `${theme.colors.alpha.black[5]}`
                        }}
                    >

                        <Profile />
                    </Box>
                </Grid>
            </Grid >
        </Container>
    )
}

export { TasksAnalytics }