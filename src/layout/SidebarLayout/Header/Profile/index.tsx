/* eslint-disable @typescript-eslint/no-explicit-any */
import { useRef, useState } from 'react'
import { Profile } from '../../../../data/Profile'
import ExpandMoreTwoToneIcon from '@mui/icons-material/ExpandMoreTwoTone';
import AccountBoxTwoToneIcon from '@mui/icons-material/AccountBoxTwoTone';
import LockOpenTwoToneIcon from '@mui/icons-material/LockOpenTwoTone';
import AccountTreeTwoToneIcon from '@mui/icons-material/AccountTreeTwoTone';
import InboxTwoToneIcon from '@mui/icons-material/InboxTwoTone';
import { Avatar, Box, Button, Divider, Hidden, List, ListItem, Popover, Tooltip, Typography, lighten, styled } from '@mui/material';
import { Link } from 'react-router-dom';

const UserBoxButton = styled(Button)(
    ({ theme }) => `
        padding-left: ${theme.spacing(1)};
        padding-right: ${theme.spacing(1)};
`
);

const MenuUserBox = styled(Box)(
    ({ theme }) => `
        background: ${theme.colors.alpha.black[5]};
        padding: ${theme.spacing(2)};
`
);

const UserBoxText = styled(Box)(
    ({ theme }) => `
        text-align: center;
        color: ${theme.colors.alpha.black[100]}
    `
);

const UserBoxLabel = styled(Typography)(
    ({ theme }) => `
        font-weight: ${theme.typography.fontWeightBold};
        color: ${theme.palette.secondary.main};
        display: block;
`
);

const UserBoxDescription = styled(Typography)(
    ({ theme }) => `
        color: ${lighten(theme.palette.secondary.main, 0.5)}
`
);

function HeaderProfile() {
    const ref = useRef<any>(null)

    const [isOpen, setIsOpen] = useState<boolean>(false)


    const handleOpen = (): void => {
        setIsOpen(true)
    }

    const handleClose = (): void => {
        setIsOpen(false)
    }
    return (
        <>
            <Tooltip
                arrow
                title="view profile"
            >
                <UserBoxButton
                    color='primary'
                    onClick={handleOpen}
                    ref={ref}
                >
                    <Avatar
                        variant='rounded'
                        alt={Profile.name}
                        src={Profile.avatar}
                    />
                    <Hidden mdDown>
                        <UserBoxText>
                            <UserBoxLabel
                                variant='body1'
                            >
                                {Profile.name}
                            </UserBoxLabel>
                            <UserBoxDescription variant='body2'>
                                {Profile.jobtitle}
                            </UserBoxDescription>
                        </UserBoxText>
                    </Hidden>

                    <Hidden
                        smDown
                    >
                        <ExpandMoreTwoToneIcon sx={{ ml: 1 }} />
                    </Hidden>
                </UserBoxButton>
            </Tooltip>

            <Popover
                anchorEl={ref.current}
                open={isOpen}
                onClose={handleClose}
                anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'right'
                }}
                transformOrigin={{
                    vertical: 'top',
                    horizontal: 'right'
                }}
            >
                <MenuUserBox
                    sx={{ minWidth: 210 }}
                    display='flex'
                    flexDirection='column'
                    justifyContent='center'
                    gap={2}

                >
                    <Avatar
                        alt={Profile.name}
                        src={Profile.avatar}
                        variant='rounded'
                        sx={{
                            width: 200,
                            height: 200
                        }}
                    />
                    <UserBoxText>
                        <UserBoxLabel
                            variant='body1'
                        >
                            {Profile.name}
                        </UserBoxLabel>
                        <UserBoxDescription variant='body2'>
                            {Profile.jobtitle}
                        </UserBoxDescription>
                    </UserBoxText>
                    <Divider />
                    <List
                        sx={{
                            p: 1
                        }}
                        component='nav'
                    >
                        <Link to='/profile'>
                            <ListItem
                                button
                                sx={{
                                    display: 'flex',
                                    justifyContent: 'space-between',
                                    alignItems: 'center'
                                }}
                            >
                                <AccountBoxTwoToneIcon fontSize='small' />
                                <Typography variant='subtitle2'>My profile</Typography>
                            </ListItem>
                        </Link>
                        <Link to='/messenger'>
                            <ListItem
                                button
                                sx={{
                                    display: 'flex',
                                    justifyContent: 'space-between',
                                    alignItems: 'center'
                                }}
                            >
                                <InboxTwoToneIcon fontSize='small' />
                                <Typography variant='subtitle2'>Messenger</Typography>
                            </ListItem>
                        </Link>
                        <Link to='/settings'>
                            <ListItem
                                button
                                sx={{
                                    display: 'flex',
                                    justifyContent: 'space-between',
                                    alignItems: 'center'
                                }}
                            >
                                <AccountTreeTwoToneIcon fontSize='small' />
                                <Typography variant='subtitle2'>Account Settings</Typography>
                            </ListItem>
                        </Link>
                    </List>
                    <Divider />
                    <Box sx={{ m: 1 }}>
                        <Button color="primary" fullWidth>
                            <LockOpenTwoToneIcon sx={{ mr: 1 }} />
                            Sign out
                        </Button>
                    </Box>
                </MenuUserBox>
            </Popover>
        </>
    )
}

export default HeaderProfile