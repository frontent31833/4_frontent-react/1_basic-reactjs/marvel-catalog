// import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App.tsx'
import AppThemeProvider from './theme/ThemeProvider'
import { ThemeProvider } from './Context/ThemeContext'
import '@fontsource/roboto/300.css';
import '@fontsource/roboto/400.css';
import '@fontsource/roboto/500.css';
import '@fontsource/roboto/700.css';
import { CssBaseline } from '@mui/material';
// import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';
// import LocalizationProvider from '@mui/lab/LocalizationProvider';
// import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { createBrowserRouter, RouterProvider } from 'react-router-dom'
import './index.css'
import { Manage } from './pages/dashboards/manage/index.tsx';
import Messenger from './pages/dashboards/messenger/index.tsx';
const routes = createBrowserRouter([
  {
    path: '/',
    element: <App />,
    children: [
      {
        index: true,
        element: <Manage />
      },
      {
        path: '/management',
        element: <Manage />
      },
      {
        path: '/messenger',
        element: <Messenger />
      }
    ]
  }
])
ReactDOM.createRoot(document.getElementById('root')!).render(
  <>
    <ThemeProvider>
      <AppThemeProvider>
        <CssBaseline />
        {/* <App /> */}
        <RouterProvider router={routes} />
      </AppThemeProvider>
    </ThemeProvider>
  </>,
)
