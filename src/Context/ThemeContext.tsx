/* eslint-disable @typescript-eslint/no-explicit-any */
import { createContext, useState, ReactNode } from 'react'
import { LightSpacesTheme } from '../theme/schemes/Light'
// import { DarkSpacesTheme } from '../theme/schemes/Dark'
// import { Theme } from '@mui/material';
import { themeMap } from '../theme/base'
const ThemeContext = createContext<any>(undefined)


function ThemeProvider({ children }: { children: ReactNode }) {
    const [themeScheme, setThemeScheme] = useState(LightSpacesTheme);
    function themeCreator(theme: string): void {
        setThemeScheme(themeMap[theme]);
    }
    const contextValue = { themeScheme, setThemeScheme, themeCreator }
    return (
        <ThemeContext.Provider value={contextValue}>
            {children}
        </ThemeContext.Provider>
    )
}

export { ThemeProvider, ThemeContext }