/* eslint-disable @typescript-eslint/no-explicit-any */
import { useState, ReactNode, createContext } from 'react';
type SidebarContext = {
    sidebarToggle: any;
    toggleSidebar: () => void;
    closeSidebar: () => void;
};
interface PropModel {
    children: ReactNode
}

export const SidebarContext = createContext<SidebarContext>(
    {} as SidebarContext
);
export function SidebarProvider({ children }: PropModel) {
    const [sidebarToggle, setSidebarToggle] = useState(false)

    const toggleSidebar = () => {
        setSidebarToggle(!sidebarToggle)
    }

    const closeSidebar = () => {
        setSidebarToggle(false)
    }

    const contextValue = { sidebarToggle, toggleSidebar, closeSidebar }
    return (
        <SidebarContext.Provider value={contextValue}>
            {children}
        </SidebarContext.Provider>
    )
}

